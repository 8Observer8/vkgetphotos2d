﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SocialManager : MonoBehaviour
{
    public Text output;

    void Start()
    {
        GetPhotos();
    }

    private void GetPhotos()
    {
        Application.ExternalCall("GetPhotos");
    }

    #region CallBacks
    public void OnGetPhotos(string str)
    {
        output.text = str;
    }
    #endregion
}
